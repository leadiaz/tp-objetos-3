import ar.edu.unq.o3.{Habitacion, Humanoide, Investigador, Monstruo}
import org.scalatest.FlatSpec

class HumanoideSpec extends FlatSpec  {


  "Un humanoide" should "perder un punto de cordura cuando es atacado "in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val investigador = new Investigador(habitacion,100,10)
    val humanoide = new Humanoide(habitacion,55,12)
    habitacion.entrarEnLaHabitacion(investigador)
    habitacion.entrarEnLaHabitacion(humanoide)
    investigador.atacar()
    assert(humanoide.cordura== 11)
  }

  "Un humanoide" should "atacar al investigador mas fuerte "in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val investigador1 = new Investigador(habitacion,100,10)
    val investigador2 = new Investigador(habitacion,77,10)
    val humanoide = new Humanoide(habitacion,55,12)
    habitacion.entrarEnLaHabitacion(investigador1)
    habitacion.entrarEnLaHabitacion(investigador2)
    habitacion.entrarEnLaHabitacion(humanoide)
    humanoide.atacar()
    assert(investigador1._puntosDeVida<100)
  }

  "un humanoide" should "deberia causar horror a todos los investigadores de la misma habitacion" in {
    val habitacion = new Habitacion (Nil,Nil)
    val otraHabitacion = new Habitacion (Nil,Nil)


    val investigadores = List(new Investigador(habitacion,100,5),
      new Investigador(habitacion,100,4),
      new Investigador(otraHabitacion,100,5))
    habitacion.investigadores.++(investigadores)
    val  humanoide = new Humanoide(habitacion,55,33)
    habitacion.entrarEnLaHabitacion(humanoide)
    assert(investigadores(0).cordura == 5)
    assert(investigadores(1).cordura == 4)
    assert(investigadores(2).cordura == 5)
  }
}
