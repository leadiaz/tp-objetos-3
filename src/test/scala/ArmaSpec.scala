import ar.edu.unq.o3.{Arma, ArmaDeFuego, ArmaFisica, Bestia, DanioAOtros, DanioPropio, DanioReducido, Desgaste, DesgasteDeNUsos, DesgastePaulatino, Fragil, Habitacion, Hechizo, Humanoide, Investigador, MansionesUtils, Monstruo}
import org.scalatest.FlatSpec

class ArmaSpec   extends FlatSpec {

  "un arma de fuego"should "daniar en 10 puntos a oponente" in {
    val habitacion = new Habitacion(Nil, Nil)
    val monstruo = new Bestia(habitacion, 100)
    val investigador = new Investigador(habitacion, 10, 10)
    val arma = new ArmaDeFuego(10)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    investigador.usarArma(arma)
    investigador.atacar()
    assert(monstruo._puntosDeVida == 90)
  }



  "Un arma de esfuerzo fisico" should "dañar 1.5 de puntos de vida del personaje"in {
    val habitacion = new Habitacion (Nil,Nil)
    val monstruo = new Bestia(habitacion,100)
    val investigador = new Investigador (habitacion,10,10)
    val arma = new ArmaFisica()
    MansionesUtils.setInitial(10)
    investigador.usarArma(arma)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    investigador.atacar()
    assert(monstruo._puntosDeVida==85)//10 * 1.5 es 15, 100 - 15 es 85
  }

  "Un arma de fuego maldita" should "ocacionar un daño reducido en un punto al personaje"in {
    val habitacion = new Habitacion (Nil,Nil)
    val monstruo = new Bestia(habitacion,100)
    val investigador = new Investigador (habitacion,10,10)
    val arma = new ArmaDeFuego(10) with DanioReducido
    investigador.usarArma(arma)
    MansionesUtils.setInitial(10)

    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    investigador.atacar()
    assert(monstruo._puntosDeVida ==91)//danio fijo de 10
  }
  "Un arma de fuego maldita" should "ocacionar un daño propio en un punto a personaje "in {
    val habitacion = new Habitacion (Nil,Nil)
    val monstruo = new Bestia(habitacion,100)
    val investigador = new Investigador (habitacion,10,10)
    val arma = new ArmaDeFuego(10) with DanioPropio
    investigador.usarArma(arma)
    MansionesUtils.setInitial(10)

    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    investigador.atacar()
    assert(monstruo._puntosDeVida ==90)//danio fijo de 10
    assert(investigador._puntosDeVida == 9)
  }
  "Un arma de fuego maldita" should "ocacionar un daño a todos los personajes en habitacion "in {
    val habitacion = new Habitacion (Nil,Nil)
    val monstruo = new Bestia(habitacion,100)
    val investigador = new Investigador (habitacion,10,10)
    val investigador2 = new Investigador(habitacion, 55, 10)
    val arma = new ArmaDeFuego(10) with DanioAOtros
    investigador.usarArma(arma)
    MansionesUtils.setInitial(10)

    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    habitacion.entrarEnLaHabitacion(investigador2)
    investigador.atacar()
    assert(monstruo._puntosDeVida ==90)//danio fijo de 10
    assert(investigador2._puntosDeVida == 54)
  }

  "Un arma de fuego con uso maximo de 2" should "ocacionar 20 punto de danio al oponente" +
    " cuando se lo ataque 3 veces"in {
    val habitacion = new Habitacion (Nil,Nil)
    val monstruo = new Bestia(habitacion,100)
    val investigador = new Investigador (habitacion,10,10)
    var arma = new ArmaDeFuego(10) with DesgasteDeNUsos
    arma.cantidadMaxima = 2

    investigador.usarArma(arma)
    //println(arma.ataque())
    MansionesUtils.setInitial(10)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    investigador.atacar()
    investigador.atacar()
    investigador.atacar()
    assert(monstruo._puntosDeVida ==80)//danio fijo de 10
  }
  "un Arma con desgaste paulatino" should "ocacionar 28 punto de danio a personaje  "in {
    val habitacion = new Habitacion(Nil, Nil)
    val monstruo = new Bestia(habitacion, 100)
    val investigador = new Investigador(habitacion, 10, 10)
    var arma = new ArmaDeFuego(10) with DesgastePaulatino
    MansionesUtils.setInitial(10)
    investigador.usarArma(arma)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    investigador.atacar()
    investigador.atacar()
    assert(monstruo._puntosDeVida == 81)
  }
  "Un arma de esfuerzo fisico" should "generar 1.5 de danio extra al oponente con danio reducido "in {
    val habitacion = new Habitacion (Nil,Nil)
    val monstruo = new Bestia(habitacion,100)
    val investigador = new Investigador (habitacion,10,10)
    val arma = new ArmaFisica()with DanioReducido
    MansionesUtils.setInitial(10)
    investigador.usarArma(arma)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    investigador.atacar()
    assert(monstruo._puntosDeVida==86)//10 * 1.5 es 15, 100 - 15 es 85
  }
  "Un arma de esfuerzo fisico" should "generar 1.5 de danio extra al oponente con danio propio "in {
    val habitacion = new Habitacion (Nil,Nil)
    val monstruo = new Bestia(habitacion,100)
    val investigador = new Investigador (habitacion,10,10)
    val arma = new ArmaFisica()with DanioPropio
    MansionesUtils.setInitial(10)
    investigador.usarArma(arma)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    investigador.atacar()
    assert(monstruo._puntosDeVida==85)//10 * 1.5 es 15, 100 - 15 es 85
    assert(investigador._puntosDeVida == 9)
  }
  "Un arma de esfuerzo fisico" should "generar 1.5 de danio extra al oponente " +
    "y generar un 10% del danio total  a otros investigadores"in {
    val habitacion = new Habitacion (Nil,Nil)
    val monstruo = new Bestia(habitacion,100)
    val investigador = new Investigador (habitacion,10,10)
    val investigador2 = new Investigador(habitacion, 55, 10)
    val investigador3 =new Investigador(habitacion, 59, 10)
    val arma = new ArmaFisica() with DanioAOtros
    investigador.usarArma(arma)
    MansionesUtils.setInitial(10)

    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    habitacion.entrarEnLaHabitacion(investigador2)
    habitacion.entrarEnLaHabitacion(investigador3)
    investigador.atacar()
    assert(monstruo._puntosDeVida ==85)//danio fijo de 10
    assert(investigador2._puntosDeVida == 53)
    assert(investigador3._puntosDeVida== 57)
  }
  "Un humanoide con arma de fuego con maldicion" should "generar 10 puntos de danio al oponente " +
    "y ocacionarle un danio propio al humanoide"in {
    val habitacion = new Habitacion (Nil,Nil)
    val monstruo = new Humanoide(habitacion,35,3)
    val investigador = new Investigador (habitacion,20,10)


    val arma = new ArmaDeFuego(10) with DanioPropio
    monstruo.usarArma(arma)
    MansionesUtils.setInitial(10)

    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)

    monstruo.atacar()
    assert(investigador._puntosDeVida ==10)//danio fijo de 10
    assert(monstruo._puntosDeVida == 34)
  }
  "un Hechizo con danio fijo de 10 puntos"should
    "atacar aun monstruo con 10 puntos de vida " +
      "y ocacionarle 1 punto de danio " in {
    val habitacion = new Habitacion(Nil, Nil)
    val monstruo = new Bestia(habitacion, 10)
    val investigador = new Investigador(habitacion, 10, 10)
    val arma = new Hechizo(10)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    MansionesUtils.setInitial(10)
    investigador.usarArma(arma)
    investigador.atacar()
    assert(monstruo._puntosDeVida == 9)
  }
  "un Hechizo con danio propio y  con danio fijo de 10 puntos"should
    "atacar aun monstruo con 10 puntos de vida " +
      "y ocacionarle 1 punto de danio " +
      "y tambien daniar en un punto al atacante" in {
    val habitacion = new Habitacion(Nil, Nil)
    val monstruo = new Bestia(habitacion, 10)
    val investigador = new Investigador(habitacion, 10, 10)
    val arma = new Hechizo(10) with DanioPropio
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    MansionesUtils.setInitial(10)
    investigador.usarArma(arma)
    investigador.atacar()
    assert(monstruo._puntosDeVida == 9)
    assert(investigador._puntosDeVida == 9)
  }
  "un Hechizo con danio reducido y con danio fijo de 20 puntos"should
    "atacar aun monstruo con 10 puntos de vida " +
      "y ocacionarle 1 punto de danio " in {
    val habitacion = new Habitacion(Nil, Nil)
    val monstruo = new Bestia(habitacion, 10)
    val investigador = new Investigador(habitacion, 10, 10)
    val arma = new Hechizo(20) with DanioReducido
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    MansionesUtils.setInitial(10)
    investigador.usarArma(arma)
    investigador.atacar()
    assert(monstruo._puntosDeVida == 9)
  }
  "un Hechizo con danio a otros y con danio fijo de 50 puntos"should
    "atacar aun monstruo con 10 puntos de vida " +
      "y ocacionarle un 10% del danio total  a todos los investigadores de la habitacion" in {
    val habitacion = new Habitacion(Nil, Nil)
    val monstruo = new Bestia(habitacion, 10)
    val investigador = new Investigador(habitacion, 10, 10)
    val aliado = new Investigador(habitacion, 10,10)
    val arma = new Hechizo(50) with DanioAOtros
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    habitacion.entrarEnLaHabitacion(aliado)

    MansionesUtils.setInitial(10)
    investigador.usarArma(arma)
    investigador.atacar()
    assert(monstruo._puntosDeVida == 5)
    assert(aliado._puntosDeVida == 9 )
  }
  "un Hechizo con danio fijo de 10 puntos"should
    "atacar aun monstruo con 4 puntos de cordura" +
      "y ocacionarle 2 punto de danio " in {
    val habitacion = new Habitacion(Nil, Nil)
    val monstruo = new Humanoide(habitacion, 10,4)
    val investigador = new Investigador(habitacion, 10, 10)
    val arma = new Hechizo(10)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    MansionesUtils.setInitial(10)
    investigador.usarArma(arma)
    investigador.atacar()
    assert(monstruo._puntosDeVida == 8)
  }

  "un Arma Fragil" should "tener un 5 % de chances de no hacer nada"in {
    val habitacion = new Habitacion(Nil, Nil)
    val monstruo = new Bestia(habitacion, 100)
    val investigador = new Investigador(habitacion, 10, 10)
    var arma = new ArmaDeFuego(10) with Fragil
    MansionesUtils.setInitial(10)
    investigador.usarArma(arma)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    investigador.atacar()
    assert(monstruo._puntosDeVida == 90)//uso #1
    investigador.atacar()
    assert(monstruo._puntosDeVida == 80)//uso #2
    investigador.atacar()
    assert(monstruo._puntosDeVida == 70)//uso #3
    investigador.atacar()
    assert(monstruo._puntosDeVida == 60)//uso #4
    investigador.atacar()
    assert(monstruo._puntosDeVida == 60)//uso #5 <- fragil! no hizo nada
    investigador.atacar()
    assert(monstruo._puntosDeVida == 50)//uso #6
    investigador.atacar()
    assert(monstruo._puntosDeVida == 40)//uso #7
    investigador.atacar()
    assert(monstruo._puntosDeVida == 30)//uso #8
    investigador.atacar()
    assert(monstruo._puntosDeVida == 20)//uso #9
    investigador.atacar()
    assert(monstruo._puntosDeVida == 20)//uso #10 <- fragil! tampoco hizo nada
  }
}
