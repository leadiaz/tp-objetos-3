import ar.edu.unq.o3.{Arcano, Bestia, Habitacion, Investigador, Monstruo}
import org.scalatest.FlatSpec

class MonstruoSpec  extends FlatSpec {
/*
    "Un monstruo" should "inicializarse con puntos de vida "in {
      val habitacion = new Habitacion (Nil,Nil)
      val monstruo = new Monstruo(habitacion,100)
      assert(monstruo._puntosDeVida.equals(100))
    }

*/
    "una bestia" should "atacar a un investigador de la misma habitacion" in {
      val habitacion = new Habitacion (Nil,Nil)

      val investigador = new Investigador(habitacion,100,10)
      val bestia = new Bestia(habitacion,70)
      habitacion.entrarEnLaHabitacion(investigador)
      habitacion.entrarEnLaHabitacion(bestia)
      bestia.atacar
      assert (investigador._puntosDeVida == 99)

    }

    /*"un monstruo" should "no atacar a un investigador de otra habitacion" in {
      val habitacion = new Habitacion (Nil,Nil)
      val otraHabitacion = new Habitacion (Nil,Nil)


      val investigador1 = new Investigador(habitacion,100,10)
      val investigador2 = new Investigador(otraHabitacion,100,10)
      val mostruo = new Monstruo(otraHabitacion,70)
      habitacion.entrarEnLaHabitacion(investigador1)
      otraHabitacion.entrarEnLaHabitacion(investigador2)
      otraHabitacion.entrarEnLaHabitacion(mostruo)

      val cuantoTiene =  investigador1.puntosDeVida
      mostruo.atacar
      assert (investigador1.puntosDeVida == cuantoTiene)

    }*/
    "un moustro" should "deberia causar horror a todos los investigadores de la misma habitacion" in {
      val habitacion = new Habitacion (Nil,Nil)
      val otraHabitacion = new Habitacion (Nil,Nil)


      val investigadores = List(new Investigador(habitacion,100,5),
                                new Investigador(habitacion,100,4),
                                new Investigador(otraHabitacion,100,5))
      habitacion.investigadores.++(investigadores)
      val  monstruo = new Bestia(habitacion,55)
      habitacion.entrarEnLaHabitacion(monstruo)
      assert(investigadores(0).cordura == 5)
      assert(investigadores(1).cordura == 4)
      assert(investigadores(2).cordura == 5)//ver!!
    }
  /*"un moustro" should "deberia atacar a todos los investigadores de la misma habitacion" in {
    val investigadores = List(new Investigador(1,100,5),
      new Investigador(1,100,4),
      new Investigador(0,100,5))
    val  bestia = new Bestia(1,55)
    bestia.atacarEnHabitacion(investigadores)
    assert(investigadores(0).puntosDeVida == 99)
    assert(investigadores(1).puntosDeVida == 99)
    assert(investigadores(2).puntosDeVida == 100)*/

  "un arcano" should "ocacionar 6 puntos de danio a investigador" in {
    var habitacion = new Habitacion(Nil, Nil)
    var arcano = new Arcano(habitacion, 55)
    var investigador = new Investigador(habitacion,66, 3)

    var investigadores = List(new Investigador(habitacion,55,5),
                              new Investigador(habitacion,88, 4),
                              investigador)

    habitacion.investigadores = investigadores


    habitacion.monstruos = arcano :: habitacion.monstruos
    arcano.atacar()
    assert(investigador._puntosDeVida == 60)
  }

  "un arcano" should "tener un 25% de posibilidades de enloquecer a todos los investigadores" in {
    var habitacion = new Habitacion(Nil, Nil)
    var arcano1 = new Arcano(habitacion, 55)
    var bestia1 = new Bestia(habitacion,50)
    var arcano2 = new Arcano(habitacion,52)
    var bestia2 = new Bestia(habitacion,49)
    var investigador = new Investigador(habitacion,66, 10)

    var investigadores = List(new Investigador(habitacion,55,8),
      new Investigador(habitacion,88, 7),
      investigador)
    habitacion.investigadores = investigadores
    habitacion.entrarEnLaHabitacion(bestia2)
    habitacion.entrarEnLaHabitacion(arcano1)
    habitacion.entrarEnLaHabitacion(bestia1)
    habitacion.entrarEnLaHabitacion(arcano2)
    //assert(investigador.cordura == 0)
    assert(investigadores.forall(x=>x.cordura == 0))
  }

}
