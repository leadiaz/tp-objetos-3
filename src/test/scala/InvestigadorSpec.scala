import ar.edu.unq.o3.{Arcano, ArmaFisica, ArtistaMarcial, Berserker, Bestia, Cobarde, Curandero, DanioReducido, DesgastePaulatino, Habitacion, Inestable, Investigador, MansionesUtils, Maton, Monstruo, Personaje}
import org.scalatest.FlatSpec



class InvestigadorSpec extends FlatSpec  {



  "Un investigador" should "inicializarse con puntos de vida "in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val investigador = new Investigador(habitacion,100,10)
    habitacion.entrarEnLaHabitacion(investigador)
    assert(habitacion.investigadores.size == 1)
    assert(investigador._puntosDeVida.equals(100))
  }
  "un investigador" must "inicializarse con puntos de cordura" in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val investigador = new Investigador(habitacion,100, 10)
    assert(investigador.cordura.equals(10))
  }

  "un investigador" should "estar cuerdo" in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val investigador = new Investigador(habitacion,100,10)
    assert(investigador.estaCuerdo)
  }



  "un investigador" should "estar menos cuerdo" in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val investigador = new Investigador(habitacion,100,10)
    investigador.perderUnPuntoDeCordura
    assert (investigador.cordura == 9)

  }

  "un investigador" should "atacar a un monstruo de la misma habitacion" in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val investigador = new Investigador(habitacion,100,10)
    val monstruo = new Bestia(habitacion,70)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    MansionesUtils.setInitial(5)
    investigador.atacar()

    assert (monstruo._puntosDeVida == 65)

  }

  "un investigador" should "no atacar a un mostruo de otra habitacion" in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val otraHabitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val investigador = new Investigador(habitacion,100,10)
    val mostruo1 = new Bestia(otraHabitacion,70)
    val mostruo2 = new Bestia(habitacion,25)

    habitacion.entrarEnLaHabitacion(mostruo2)
    habitacion.entrarEnLaHabitacion(investigador)
    otraHabitacion.entrarEnLaHabitacion(mostruo1)
    MansionesUtils.setInitial(5)
    investigador.atacar()
    assert (mostruo1._puntosDeVida == 70)
    assert(mostruo1._posicion != investigador._posicion)

  }

  "un maton" should "recuperar sus puntos de cordura si derrota a un oponente" in {
    val habitacion = new Habitacion(Nil,Nil)
    val maton = new Investigador(habitacion,50,5) with Maton
    val monstruo = new Bestia(habitacion, 5)
    MansionesUtils.setInitial(5)
    habitacion.entrarEnLaHabitacion(maton)
    habitacion.entrarEnLaHabitacion(monstruo)
    maton.atacar()
    assert(monstruo._puntosDeVida == 0)
    assert(maton.cordura == 10)
  }

  "un artista Marcial" should "ocasionar un 50% más de danio extra a oponente" in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val artMarcial = new Investigador(habitacion,100,5)with ArtistaMarcial// revisar parametros
    val monstruo = new Bestia(habitacion,35)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(artMarcial)
    MansionesUtils.setInitial(5)

    artMarcial.atacar
    assert(monstruo._puntosDeVida== 28)//ver!!
  }

  "un berserker" should "ocasionar el doble de danio a si mismo porque esta loco" in {
    val habitacion = new Habitacion (Nil,Nil)
    val berserker = new Investigador(habitacion,100,0)with Berserker// ver parámetros y clase

    habitacion.entrarEnLaHabitacion(berserker)
    MansionesUtils.setInitial(5)

    berserker.atacar
    assert(!berserker.estaCuerdo)
    assert(berserker._puntosDeVida== 90)

    }

  "un cobarde" should "perder un punto de cordura al atacar" in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val cobarde = new Investigador(habitacion,100,1) with Cobarde// ver parámetros y clase
    val monstruo = new Bestia(habitacion,35)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(cobarde)
    MansionesUtils.setInitial(5)
    cobarde.atacar
    //cobarde.atacarA(monstruo)
    assert(cobarde.cordura == 0)
  }
  "un atacante inestable" should "perder un punto de puntos de vida cuando su cordura baje" in {
    val habitacion = new Habitacion (List.empty[Investigador],List.empty[Monstruo])
    val inestable = new Investigador(habitacion,100,2) with Inestable// ver parámetros y clase
    inestable.perderUnPuntoDeCordura
    MansionesUtils.setInitial(5)
    assert(inestable._puntosDeVida == 99)
  }
  "un curandero cobarde" should "perder un punto de cordura al atacar y curar a aun aliado mas debil" in {
    val habitacion = new Habitacion (Nil,Nil)
    val curanderoCobarde = new Investigador(habitacion,100,3) with Cobarde with Curandero// ver parámetros y clase
    val monstruo = new Bestia(habitacion,35)
    val aliado1 = new Investigador(habitacion,50,6)
    val aliado2 = new Investigador(habitacion,44 ,5)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(curanderoCobarde)
    habitacion.entrarEnLaHabitacion(aliado1)
    habitacion.entrarEnLaHabitacion(aliado2)
    MansionesUtils.setInitial(5)
    curanderoCobarde.atacar
    curanderoCobarde.reestablecerPuntosDeVida()
    assert(curanderoCobarde.cordura == 2)
    //el segundo aliado es el mas debil
    assert(aliado2._puntosDeVida == 46)
  }

  "un ivestigador maton, berserker y cobarde" +
    " con un hacha con danio reducido y desgaste paulatino" should "daniar a un arcano en 8 puntos" in {
    val habitacion  = new Habitacion(Nil, Nil)
    val investigador = new Investigador(habitacion, 6 , 1) with Maton with Berserker with Cobarde
    val monstruo = new Arcano(habitacion,10)
    val hacha = new ArmaFisica() with DanioReducido with DesgastePaulatino
    MansionesUtils.setInitial(10)
    investigador.usarArma(hacha)
    habitacion.entrarEnLaHabitacion(monstruo)
    habitacion.entrarEnLaHabitacion(investigador)
    investigador.atacar
    assert(monstruo._puntosDeVida == 2 )
    assert(investigador.cordura == 0)


  }
 }
