package ar.edu.unq.o3

abstract class Monstruo extends Personaje with Atacante {
  override def danio: Int
  override def irAHabitacion(otra: Habitacion ) = {
    _posicion.entrarEnLaHabitacion(this)
    otra.quitarDeLaHabitacion(this)
    this.causarHorror

  }
  def causarHorror(): Unit ={
    var miedosos = this._posicion.investigadores
    miedosos.foreach(e => e.perderUnPuntoDeCordura)
  }

  override def muerto(): Unit = _posicion.quitarDeLaHabitacion(this)

  override def valorDePersonajeParaHechizo(danioArma:Int): Int = MansionesUtils.roundInt(danioArma/_puntosDeVida)

}

class Bestia (override var _posicion: Habitacion,override var _puntosDeVida: Int) extends Monstruo {

  override def danio = _posicion.cantMonstruos()
  override def oponente: Personaje = this._posicion.investigadorMasDebil()
}
class Arcano(override var _posicion: Habitacion,override var _puntosDeVida: Int)extends Monstruo{

  override def oponente: Personaje = this._posicion.investigadores.minBy(_ cordura)

  override def danio: Int = {
    if(_posicion.investigadores.length !=0){10 - MansionesUtils.roundInt(_posicion.investigadores.map(_ cordura).sum / _posicion.cantInvestigadores())}
    else{10}

  }

  //-_-_-_-_-_-_-_-_ horror del arcano
  /*
  adicionalmente, al causar los arcanos horror a los investigadores, tendrán una posibilidad del 25%
  de sumergirlos directemente en la locura (restarle todos los puntos de cordura disponibles!)
   */
  override def causarHorror(): Unit = {
    //var porcentaje = MansionesUtils.randomIntBetween(1,100)
    //if (porcentaje <=25){
    var cuarteto = this._posicion.monstruos.length +1 //a fines de poder testear
    if (cuarteto ==4){//solo a fines de test, si hay 4 monstruos, el investigador enloquece
      var pobretipo = this._posicion.investigadores
      pobretipo.foreach(e=> e.enloquecer)
    }
    else{
      super.causarHorror()
    }
  }

}