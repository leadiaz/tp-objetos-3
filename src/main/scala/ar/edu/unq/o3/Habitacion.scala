package ar.edu.unq.o3

class Habitacion (var investigadores: List[Investigador], var monstruos: List[Monstruo]){


  def todos():List[Personaje] = investigadores ++ monstruos
  def cantInvestigadores() = investigadores.size
  def cantMonstruos() = this.monstruos.size
  def investigadorMasDebil() = investigadores.minBy(p=> p._puntosDeVida)
  def investigadorMasFuerte() = investigadores.maxBy(p=>p._puntosDeVida)
  def monstroMasFuerte() = monstruos.maxBy(p=>p._puntosDeVida)
  //var numeroAlAzar= MansionesUtils.randomIntBetween(1,99)
  def selectCualquiera():Personaje = {MansionesUtils.randomElemnt(this.todos())
    /*
    defino un criterio para saber de que lista sacar un elemento
    por ejemplo sacar un numero, si es par es un monstruo, sino es un investigador
    luego sacar algo de esa lista aleatoriamente

    if (numeroAlAzar % 2 == 0){
      this.numeroAlAzar = MansionesUtils.randomIntBetween(1,99)
      MansionesUtils.randomElemnt(this.monstruos)

    }
    else{
      this.numeroAlAzar = MansionesUtils.randomIntBetween(1,99)
      MansionesUtils.randomElemnt(this.investigadores)
    }*/
  }

  /*
  de la habitacion se puede entrar o salir
   */
  def entrarEnLaHabitacion(unMonstruo:Monstruo): Unit ={
    val listaConUnMonstruoMas = unMonstruo :: this.monstruos
    unMonstruo._posicion=this
    //espantar a los investigadores
    unMonstruo.causarHorror()
    this.monstruos = listaConUnMonstruoMas

  }

  def entrarEnLaHabitacion(unInvestigador:Investigador)={
    val listaConUnInvestigadorMas = unInvestigador :: this.investigadores
    unInvestigador._posicion = this
    this.investigadores = listaConUnInvestigadorMas
  }

  def quitarDeLaHabitacion(unMostruo:Monstruo): Unit ={
    this.monstruos.filter(_ != unMostruo)
  }

  def quitarDeLaHabitacion(unInvestigador:Investigador): Unit ={
    this.investigadores.filter(_ != unInvestigador)
  }

  def entrarTodosLosInvestigadores(filaDeInvestigadores:List[Investigador]): Unit ={
    for (cadaUno <- filaDeInvestigadores if cadaUno._posicion == this){
      this.entrarEnLaHabitacion(cadaUno)
    }
  }

  def entrarTodosLosMonstruos(filaDeMonstruos:List[Monstruo]): Unit ={
    for (cadaUno <- filaDeMonstruos if cadaUno._posicion == this){
      this.entrarEnLaHabitacion(cadaUno)
    }
  }

  def todosMenosYo (esePersonaje: Personaje) ={
    this.todos().filter(p => p != esePersonaje)
  }

}
