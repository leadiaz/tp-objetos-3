package ar.edu.unq.o3

abstract class Arma () {
  def danio(personaje:Personaje):Int = personaje._puntosDeVida
}
class ArmaFisica()extends Arma(){
 override def danio(personaje: Personaje) = MansionesUtils.roundInt(1.5 * personaje._puntosDeVida)
}

class ArmaDeFuego(danioFijo:Int) extends Arma {

  override def danio(personaje: Personaje): Int = danioFijo
}


class Hechizo (_danio: Int)extends Arma {
  override def danio(personaje: Personaje): Int = personaje.oponente.valorDePersonajeParaHechizo(_danio)
}

//}


trait DanioReducido extends Arma {

  override def danio(personaje: Personaje): Int = {
    super.danio(personaje) - 1
  }
}
trait DanioPropio extends Arma {

  override def danio(personaje: Personaje): Int = {
    val danio:Int = super.danio(personaje)
    personaje.recibirDanio(1)
    return danio
  }
}
trait DanioAOtros extends Arma {
  override def danio(personaje: Personaje): Int={
    val aliados = personaje._posicion.investigadores.filter(_ != personaje)
    aliados.foreach(p =>p.recibirDanio(MansionesUtils.roundInt(super.danio(personaje) * 0.1)))
    super.danio(personaje)
  }
}


trait Desgaste extends Arma {
  var cantidadUsada : Int = 0
   //override var _danio: Int = _
}

trait DesgasteDeNUsos extends Desgaste{
  var cantidadMaxima:Int = 0

  override def danio(personaje: Personaje): Int = {
    if (cantidadUsada<cantidadMaxima){
      cantidadUsada += 1
      super.danio(personaje)

    }
    else{
      0
    }
  }
}

trait DesgastePaulatino extends Desgaste {

  override def danio(personaje: Personaje): Int = {

    val porcentaje = 100 - (10 * cantidadUsada)
    if (porcentaje > 5) {
      cantidadUsada += 1
      MansionesUtils.roundInt(super.danio(personaje) * porcentaje / 100)

    }
    else {
      MansionesUtils.roundInt(super.danio(personaje) * 50 / 100)
    }

  }

}

//-_-_-_-_-_-_-_-_-_a partir de aqui modificaciones -_-_-_-_-_-_-_

/*
Desgaste -> Fragil
El arma u objeto es tan fragil que no se sabe como se va a comportar.
Ante cada uso hay un 5% de chances que no haga nada.

Se define la probabilidad del suceso A como:
P(A) = N° de casos favorables al suseso A / N° Total de Casos posibles
5/100 =
 */

trait Fragil extends Desgaste{
  override def danio(personaje: Personaje): Int = {
    cantidadUsada += 1
    //val probabilidad =  cantidadUsada + (MansionesUtils.randomIntBetween(0,100))//algo aleatorio
    //if (probabilidad > 5) { //existe un 5% de chances que el numero resultante sea menor a 5 en un intervalo de 1 a 100
    val muyProbable = cantidadUsada % 5 //cada 5 usos el arma es fragil  <- solo para poder testear!!!
    if (muyProbable != 0){
      super.danio(personaje)
    }
    else {
      0
    }

  }

}