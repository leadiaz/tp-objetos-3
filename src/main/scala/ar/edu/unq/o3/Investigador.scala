package ar.edu.unq.o3
trait AtaqueInvestigador extends Personaje with Atacante with ConCordura {
  var armaActual: Arma
  def seleccionarAlMasFuerte: Personaje ={
    this._posicion.monstroMasFuerte()
  }

  def seleccionarCualquiera: Personaje ={
    this._posicion.selectCualquiera()
  }
  override def oponente: Personaje = {
    if(estaCuerdo){this.seleccionarAlMasFuerte}else{seleccionarCualquiera}
  }

  def danioDeArma():Int = {
    val miDanio = MansionesUtils.randomIntBetween(1,100)
    if(armaActual == null){miDanio}
    else{armaActual.danio(this)}
  }
  override def danio(): Int = {
    this.danioDeArma
  }

}

class Investigador(override var _posicion: Habitacion,override var _puntosDeVida: Int, override var cordura: Int) extends Personaje  with AtaqueInvestigador {
  //maximo nivel de cordura es 10
  override var armaActual: Arma = null
  override def muerto(): Unit = _posicion.quitarDeLaHabitacion(this)
 // override def usarArma(arma: Arma): Unit ={
   // armaActual = arma
   // super.usarArma(arma)
  //}
  def usarArma(arma: Arma)= armaActual = arma


  protected def reiniciarPuntosDeCordura()= cordura=10



  override def irAHabitacion(otraHabitacion: Habitacion): Unit = {
    _posicion.quitarDeLaHabitacion(this)
    otraHabitacion.entrarEnLaHabitacion(this)
  }

  override def valorDePersonajeParaHechizo(danioArma:Int): Int = MansionesUtils.roundInt(danioArma/(cordura+1))
}

trait Maton extends Investigador{
  override def atacar(): Unit = {
    super.atacar()
    if(!oponente.estaVivo()){reiniciarPuntosDeCordura()}
  }
}
trait ArtistaMarcial extends Investigador{
  override def danio(): Int = {
    super.danio() + (super.danio()/2)
  }
}
trait Berserker extends Investigador{
  override def danio: Int = {
    if (!estaCuerdo){
      super.danio * 2
    }
    else {
      super.danio
    }
  }
}
trait Cobarde extends Investigador{
  override def atacar: Unit = {
    super.atacar()
    this.perderUnPuntoDeCordura
  }
}
trait Inestable extends Investigador{
  override def perderUnPuntoDeCordura: Unit = {
    super.perderUnPuntoDeCordura
    this.recibirDanio(1)
  }
}
/*
class Maton (habitacion: Habitacion, puntosDeVida: Int, cordura: Int) extends Investigador (habitacion, puntosDeVida, cordura ){

  /*override def atacarA(unMonstruo: Personaje): Unit = {
    super.atacarA(unMonstruo)
    if (!unMonstruo.estaVivo){
      this.reiniciarPuntosDeCordura()
    }
  }*/
  override def atacar(): Unit = {
    super.atacar()
    if(!oponente.estaVivo()){reiniciarPuntosDeCordura()}
  }
}

class ArtistaMarcial (habitacion: Habitacion, puntosDeVida: Int, cordura: Int) extends Investigador (habitacion,puntosDeVida,cordura ) {
  /*ocacionar un 50 % de daño*/

  override def danio(): Int = {
    super.danio() + (super.danio()/2)
  }

}


class Berserker(habitacion: Habitacion, puntosDeVida: Int, cordura: Int) extends Investigador (habitacion,puntosDeVida,cordura ){
  override def danio: Int = {
    if (!estaCuerdo){
      super.danio * 2
    }
    else {
      super.danio
    }
  }
}

class Cobarde(habitacion: Habitacion, puntosDeVida: Int, cordura: Int) extends Investigador (habitacion,puntosDeVida,cordura ) {
  override def atacar: Unit = {
    super.atacar()
    this.perderUnPuntoDeCordura
  }

}

class Inestable(habitacion: Habitacion, puntosDeVida: Int, cordura: Int) extends Investigador (habitacion,puntosDeVida,cordura ){
  override def perderUnPuntoDeCordura: Unit = {
    super.perderUnPuntoDeCordura
    this.recibirDanio(1)
  }

}
*/


trait Curandero extends Investigador {


  def reestablecerPuntosDeVida() = {
    if(estaCuerdo) {this._posicion.investigadores.minBy(_ _puntosDeVida).curar(2)}
    else{this.seleccionarCualquiera.curar(2)}
  }


}
trait Martir extends Investigador {
  def reestablecerNPuntoDeCorduraAAliados(n: Int) = {
    var compas = this._posicion.investigadores.filter(_ != this)
    compas.foreach(e => e.cordura = e.cordura + n)
    this.recibirDanio(n / 2)
  }
}

