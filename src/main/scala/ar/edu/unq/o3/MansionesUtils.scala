package ar.edu.unq.o3

import scala.util.Random

object MansionesUtils {
  private var inicioR:Int = 0
  def randomIntBetween(low:Int, higt:Int)={
    if(inicioR == 0){new Random().nextInt(higt-low) + low}else{inicioR}
  }
  def roundInt(d: Double)= Math.round(d).asInstanceOf[Int]
  def randomElemnt[T](lista: List[T]):T = {
    val aux:Int = inicioR
    inicioR = 0
    val personaje: T = lista(randomIntBetween(0,lista.length))
    this.setInitial(aux)
    return personaje
  }
  def setInitial(n:Int)= inicioR= n
}
