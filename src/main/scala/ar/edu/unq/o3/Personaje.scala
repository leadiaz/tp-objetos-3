package ar.edu.unq.o3

import scala.util.Random

trait Atacante/* extends Personaje */{
  def oponente: Personaje
  def  danio(): Int
  def atacar() = {this.atacarA(this.oponente)}
  def atacarA(otroPersonaje: Personaje):Unit = {
    otroPersonaje.recibirDanio(danio())
  }
}

trait Estado {
  var _puntosDeVida: Int
  var _posicion : Habitacion
  def estaVivo(): Boolean = _puntosDeVida > 0
  def muerto(): Unit
}

trait ConCordura{
  var cordura:Int
  def estaCuerdo = cordura>0
  def perderUnPuntoDeCordura: Unit ={
    this.cordura = this.cordura-1
  }
  def enloquecer={
    this.cordura = 0
  }
}







abstract class Personaje()extends Estado with Atacante {
  /*
  ahora al ir a la habitacion, con mandarle una habitacion, le delego la responsabilidad a esa clase
   */
  def irAHabitacion(otraHabitacion: Habitacion): Unit
  def valorDePersonajeParaHechizo(danioArma:Int):Int
  def recibirDanio(puntosPerdidos: Int) {
    /*
    aca deberiamos capturar una exepcion cuando el personaje
        llegue a 0 de puntos de vida
    */
    if (_puntosDeVida >= puntosPerdidos) {
      _puntosDeVida = _puntosDeVida - puntosPerdidos
    }
    else {
      this.muerto()
      _puntosDeVida = 0
    }
  }
  def curar(n: Int) = {
    _puntosDeVida += n
  }

  /*
  tanto investigador como monstruo pueden usar una o varias armas
   */
  /*def usarArma(unArma : Arma): Unit ={
    unArma.setOwner(this)

    //unArma.danioDelPortador(unArma._danio())
  }*/

}


