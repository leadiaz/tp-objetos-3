package ar.edu.unq.o3

class Humanoide (override var _posicion: Habitacion, override var _puntosDeVida: Int, override var cordura: Int)extends Monstruo with AtaqueInvestigador {
  def usarArma(arma: Arma) = armaActual = arma

  override var armaActual: Arma = null

  override def seleccionarAlMasFuerte: Personaje = {
    this._posicion.investigadorMasFuerte()
  }

  override def recibirDanio(puntosPerdidos: Int): Unit = {
    super.recibirDanio(puntosPerdidos)
    this.perderUnPuntoDeCordura
  }
  override def valorDePersonajeParaHechizo(danioArma:Int)= MansionesUtils.roundInt(danioArma / (cordura + 1 ))

}
